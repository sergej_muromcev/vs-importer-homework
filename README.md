# vs-importer-homework

Initial steps:

* First of all, please create temporary DB on your local machine.

* Manually execute SQL provided. (/Setup/InitDbScript.sql)

* Edit /DataImportExport/DataImportExport/App.config and replace following placeholders {DB_URI} and {DB_NAME}.

* Build application and you're ready to go.
