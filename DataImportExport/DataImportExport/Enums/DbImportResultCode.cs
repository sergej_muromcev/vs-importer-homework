﻿namespace DataImportExport.Enums
{
    public enum DbImportResultCode
    {
        None = 0,
        Ok = 1,
        CodeNotFound = 2,
        UnknownError = 3

    }
    static class ImportCodeExtension
    {
        public static string ToErrorString(this DbImportResultCode code)
        {
            return $"Error occurred during DB import: {code.ToString()}";
        }
    }
}
