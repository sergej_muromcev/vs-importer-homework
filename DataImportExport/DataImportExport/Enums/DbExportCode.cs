﻿namespace DataImportExport.Enums
{
    enum DbExportCode
    {
        None = 0,
        Ok = 1,
        NoRecordUpdated = 2
    }
}
