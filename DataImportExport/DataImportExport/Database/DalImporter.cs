﻿using DataImportExport.Enums;
using DataImportExport.Filesystem;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DataImportExport.Database
{
    static class DalImporter
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(DalImporter));
        public static IEnumerable<DataToExport> GetExportData(ImporterEntities entities)
        {
            return entities.DataToExports.AsEnumerable();
        }
        public static DbImportResultCode ImportRecord(ImporterEntities entities, int customerId, string customerName, string notes, int? customerCode)
        {
            log.Debug($"{nameof(ImportRecord)} method called with parameters {customerId}, {customerName}, {notes}, {customerCode}");
            var resultSet = entities.InsertCustomerData(customerId,
               String.IsNullOrWhiteSpace(customerName) ? "" : customerName,
               String.IsNullOrWhiteSpace(notes) ? "" : notes, customerCode);
            var result = resultSet?.FirstOrDefault();

            if (result == null)
            {
                return DbImportResultCode.UnknownError;
            }
            if (result.Value.Equals(0))
            {
                return DbImportResultCode.CodeNotFound;
            }

            return DbImportResultCode.Ok;
        }

        public static DbExportCode ConfirmExport(ImporterEntities entities, int dbId)
        {
            log.Debug($"{nameof(ConfirmExport)} method called with parameter {dbId}");
            var result = entities.ConfirmDataExport(dbId);
            switch (result)
            {
                case 1: return DbExportCode.Ok;
                case 0: return DbExportCode.NoRecordUpdated;
                default: return DbExportCode.None;
            }
        }

        internal static IEnumerable<Tuple<DbImportResultCode, CustomerFileInputRecord>> ImportRecords(CustomerFileInputRecord[] records)
        {
            using (var e = new ImporterEntities())
            {
                foreach (var record in records)
                {
                    var res = ImportRecord(e, record.CustomerId, record.CustomerName, record.Notes, record.CodeId);
                    if (!DbImportResultCode.Ok.Equals(res))
                    {
                        yield return Tuple.Create(res, record);
                    }
                }
            }
        }
    }
}
