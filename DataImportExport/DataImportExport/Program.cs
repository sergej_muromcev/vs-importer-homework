﻿using CommandLine;
using CommandLine.Text;
using DataImportExport.Database;
using DataImportExport.Filesystem;
using FileHelpers;
using log4net;
using System;
using System.IO;
using System.Linq;

namespace DataImportExport
{
    class Program
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(Program));
        static void Main(string[] args)
        {
            log4net.Config.XmlConfigurator.Configure();

            var parserResult = Parser.Default.ParseArguments<Options>(args);
            if (parserResult.Tag == ParserResultType.NotParsed)
            {
                HelpText.AutoBuild(parserResult);
            }
            else
            {
                try
                {
                    parserResult.WithParsed(opts => RunOptionsAndReturnExitCode(opts));
                }
                catch (ArgumentException e)
                {
                    log.Fatal(e);
                    Console.Write(HelpText.RenderUsageText(parserResult));
                }
                catch (Exception e)
                {
                    log.Fatal(e);
                }
            }
            Console.ReadKey();
        }

        private static void RunOptionsAndReturnExitCode(Options options)
        {
            LogManager.GetRepository().Threshold = options.GetLoggingLevel();

            var importOptionSet = !String.IsNullOrWhiteSpace(options.ImportFilePath);
            var exportOptionSet = !String.IsNullOrWhiteSpace(options.ExportFilePath);
            if (importOptionSet && exportOptionSet)
            {
                throw new ArgumentException("Please select either you want to import or export data but not both");
            }
            else if (importOptionSet)
            {
                if (File.Exists(options.ImportFilePath))
                {
                    DoImport(options.ImportFilePath, options.ErrorOutputPath);
                }
                else
                {
                    throw new FileNotFoundException("Input file for input wasn't found.", options.ImportFilePath);
                }
            }
            else if (exportOptionSet)
            {
                DoExport(options.ExportFilePath);
            }
            else
            {
                throw new ArgumentException("Please select either you want to import or export data");
            }
        }

        /// <summary>
        /// A bit too lazy to decouple everything now.
        /// </summary>
        /// <param name="exportFilePath"></param>
        private static void DoExport(string exportFilePath)
        {
            log.Debug($"{nameof(DoExport)} function started with '{exportFilePath}' parameter");
            using (var entities = new ImporterEntities())
            {
                var data = DalImporter.GetExportData(entities);
                log.Info($"Selected {data.Count()} records for export.");
                if (data.Any())
                {
                    FileHelperAsyncEngine<CustomerFileExportRecord> engine = null;
                    try
                    {
#pragma warning disable CC0022 // Should dispose object. Used MS approach for disposal https://msdn.microsoft.com/library/ms182334.aspx
                        engine = new FileHelperAsyncEngine<CustomerFileExportRecord>();
#pragma warning restore CC0022 // Should dispose object
                        engine.BeginWriteFile(exportFilePath);
                        var trans = entities.Database.BeginTransaction();
                        try
                        {
                            foreach (var record in data)
                            {
                                DalImporter.ConfirmExport(entities, record.id);
                                var recToExport = new CustomerFileExportRecord(record);
                                engine.WriteNext(recToExport);
                            }
                            engine.Flush();
                            trans.Commit();
                        }
                        catch (Exception)
                        {
                            trans.Rollback();
                            engine.Close();
                            engine = null;
                            if (File.Exists(exportFilePath))
                            {
                                try { File.Delete(exportFilePath); }
                                catch (Exception)
                                {
                                    log.Warn($"Wasn't very successful deleting file '{exportFilePath}'. Please take care manually.");
                                }
                            }
                            throw;
                        }
                    }
                    finally
                    {
                        if (engine != null)
                        {
                            engine.Close();
                        }
                    }
                }
            }
        }

        private static void DoImport(string importFilePath, string errorOutputPath)
        {
            log.Debug($"{nameof(DoImport)} function started with '{importFilePath}' and '{errorOutputPath}' parameters");
            var data = FsHelper.ReadCustomerRecords(importFilePath, errorOutputPath);
            var importResult = DalImporter.ImportRecords(data);
            var additionalErrors = importResult.Select(x => new FaultedImportRecord(x.Item1, x.Item2));
            FsHelper.WriteFaultReport(errorOutputPath, additionalErrors, true);
        }
    }
}
