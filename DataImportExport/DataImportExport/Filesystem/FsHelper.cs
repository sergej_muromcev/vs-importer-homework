﻿using FileHelpers;
using log4net;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace DataImportExport.Filesystem
{
    static class FsHelper
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(FsHelper));
        public static CustomerFileInputRecord[] ReadCustomerRecords(string filepath, string errorOutputFilePath)
        {
            var engine = new DelimitedFileEngine<CustomerFileInputRecord>
            {
                ErrorMode = ErrorMode.SaveAndContinue
            };

            var customers = engine.ReadFile(filepath);
            log.Info($"Loaded {customers.Count()} records from '{filepath}'.");
            if (engine.ErrorManager.HasErrors && errorOutputFilePath != null)
            {
                log.Info($"{engine.ErrorManager.Errors.Count()} faulty lines discovered.");
                var output = engine.ErrorManager.Errors.Select(item => new FaultedImportRecord(item));
                WriteFaultReport(errorOutputFilePath, output);
            }
            return customers;
        }

        public static void WriteFaultReport(string filePath, IEnumerable<FaultedImportRecord> data, bool append = false)
        {
            using (var engine = new FileHelperAsyncEngine<FaultedImportRecord>())
            {
                if (append && File.Exists(filePath))
                {
                    engine.BeginAppendToFile(filePath);
                }
                else
                {
                    engine.BeginWriteFile(filePath);
                }
                engine.WriteNexts(data);
                log.Info($"Written {data.Count()} records to '{filePath}'.");
            }
        }
    }
}
