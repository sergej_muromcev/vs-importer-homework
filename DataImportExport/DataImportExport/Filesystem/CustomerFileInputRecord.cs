﻿using FileHelpers;
using FileHelpers.Events;

namespace DataImportExport.Filesystem
{
    [DelimitedRecord(";")]
    [IgnoreEmptyLines]
    public class CustomerFileInputRecord : INotifyRead
    {
        public int CustomerId;
        [FieldTrim(TrimMode.Both)]
        public string CustomerName;
        [FieldTrim(TrimMode.Both)]
        public string Notes;
        public int? CodeId;

        public void AfterRead(AfterReadEventArgs e)
        {
            // Method intentionally left empty.
        }

        public void BeforeRead(BeforeReadEventArgs e)
        {
            if (e.RecordLine.StartsWith("#"))
            {
                e.SkipThisRecord = true;
            }
        }
    }
}
