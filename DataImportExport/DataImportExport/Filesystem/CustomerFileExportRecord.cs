﻿using DataImportExport.Database;
using FileHelpers;
using System;

namespace DataImportExport.Filesystem
{
    [DelimitedRecord(";")]
    public class CustomerFileExportRecord
    {
        public int CustomerId;
        public string CustomerName;
        public string Notes;
        public string CodeName;
        [FieldConverter(ConverterKind.Date, "yyyy-MM-dd")]
        public DateTime ImportedDate;
        [FieldConverter(ConverterKind.Date, "yyyy-MM-dd")]
        public DateTime ExportedDate;

        public CustomerFileExportRecord() { }
        public CustomerFileExportRecord(DataToExport record)
        {
            CustomerId = record.CustomerId;
            CustomerName = record.CustomerName;
            Notes = record.Notes;
            CodeName = record.CodeName;
            ImportedDate = record.ImportedDate;
            ExportedDate = DateTime.Today; // Purposely generating DateTime separately to keep logics simple.
        }
    }
}
