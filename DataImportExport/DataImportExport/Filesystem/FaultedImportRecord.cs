﻿using DataImportExport.Enums;
using FileHelpers;

namespace DataImportExport.Filesystem
{
    [DelimitedRecord("\t")]
    public class FaultedImportRecord
    {
        public string ErrorOccured;
        public int OriginalInputLineNumber;
        public string OriginalInputLine;

        public FaultedImportRecord() { }
        public FaultedImportRecord(ErrorInfo item)
        {
            ErrorOccured = item.ExceptionInfo.Message;
            OriginalInputLineNumber = item.LineNumber;
            OriginalInputLine = item.RecordString;
        }

        public FaultedImportRecord(DbImportResultCode code, CustomerFileInputRecord data)
        {
            ErrorOccured = code.ToErrorString();
            var engine = new FileHelperEngine<CustomerFileInputRecord>();
            OriginalInputLine = engine.Options.RecordToString(data);
        }
    }
}
