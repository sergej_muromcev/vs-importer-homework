﻿using CommandLine;
using CommandLine.Text;
using log4net.Core;
using System.Collections.Generic;

namespace DataImportExport
{
    [Verb("head", HelpText = "Allows data importing from CSV to Database and exporting it back.")]
    public class Options
    {
        [Option('i', "import", Required = false, HelpText = "File path for import")]
        public string ImportFilePath { get; set; }

        [Option('e', "export", Required = false, HelpText = "File path for export")]
        public string ExportFilePath { get; set; }

        [Option('l', "log", Required = false, Default = "./faulted_processing.log", HelpText = "File path for logging ")]
        public string ErrorOutputPath { get; set; }

        [Option('v', Default = false, HelpText = "Prints all messages to standard output.")]
        public bool Verbose { get; set; }

        [Option('d', Default = false, HelpText = "Prints debug messages to standard output.")]
        public bool Debug { get; set; }

        [Option('s', Default = false, HelpText = "Prints debug messages to standard output.")]
        public bool Silence { get; set; }

        [Usage(ApplicationAlias = "DataImportExport.exe")]
        public static IEnumerable<Example> Examples
        {
            get
            {
                yield return new Example("import scenario", new Options { ImportFilePath = @"C:\folder\file.txt" });
                yield return new Example("import scenario with more control and output", new Options { ImportFilePath = @"C:\folder\file1.txt", ErrorOutputPath = @"C:\folder\file2.txt", Verbose = true });
                yield return new Example("export scenario", new Options { ExportFilePath = @"C:\folder\file.txt" });
            }
        }
    }
    public static class OptionsExtension
    {
        public static Level GetLoggingLevel(this Options options)
        {
            if (options.Verbose)
            {
                return Level.All;
            }
            if (options.Debug)
            {
                return Level.Debug;
            }
            if (options.Silence)
            {
                return Level.Off;
            }
            return Level.Info;
        }
    }
}
