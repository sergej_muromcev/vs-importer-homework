/****** Object:  Table [dbo].[CodeLookup]    Script Date: 5/17/2018 02:16:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CodeLookup](
	[id] [int] NOT NULL,
	[Name] [varchar](200) NOT NULL,
 CONSTRAINT [PK_CodeLookup] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CustomerData]    Script Date: 5/17/2018 02:16:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CustomerData](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[CustomerId] [int] NOT NULL,
	[CustomerName] [varchar](20) NOT NULL,
	[Notes] [varchar](200) NULL,
	[Code] [int] NULL,
	[ImportedDate] [datetime] NOT NULL,
	[ExportedDate] [datetime] NULL,
 CONSTRAINT [PK_CustomerData] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[DataToExport]    Script Date: 5/17/2018 02:16:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[DataToExport]
AS
SELECT        d.id, d.CustomerId, d.CustomerName, d.Notes, c.Name AS CodeName, d.ImportedDate
FROM            dbo.CustomerData AS d LEFT OUTER JOIN
                         dbo.CodeLookup AS c ON d.Code = c.id
WHERE        (d.ExportedDate IS NULL)
GO
ALTER TABLE [dbo].[CustomerData] ADD  CONSTRAINT [DF_CustomerData_ImportedDate]  DEFAULT (getdate()) FOR [ImportedDate]
GO
ALTER TABLE [dbo].[CustomerData]  WITH NOCHECK ADD  CONSTRAINT [FK_CustomerData_CodeLookup] FOREIGN KEY([Code])
REFERENCES [dbo].[CodeLookup] ([id])
GO
ALTER TABLE [dbo].[CustomerData] CHECK CONSTRAINT [FK_CustomerData_CodeLookup]
GO
/****** Object:  StoredProcedure [dbo].[ConfirmDataExport]    Script Date: 5/17/2018 02:16:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ConfirmDataExport] 
	@id int
AS
BEGIN
	SET NOCOUNT ON;
	update CustomerData
	set ExportedDate = getdate()
	where id = @id;

	return cast(@@ROWCOUNT-1 as int);
	
END
GO
/****** Object:  StoredProcedure [dbo].[InsertCustomerData]    Script Date: 5/17/2018 02:16:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[InsertCustomerData]
@CustomerId int,
@CustomerName varchar(20),
@Notes varchar(200),
@Code int
AS
BEGIN
	SET NOCOUNT ON;
	IF (@Code is null or (select COUNT(*) from CodeLookup where id = @Code)=1)
	begin
		insert into CustomerData(CustomerId, CustomerName, Notes, Code) values (@CustomerId, @CustomerName, @Notes, @Code)
		select CAST(SCOPE_IDENTITY() AS int) 
	end
	else
	begin
		select CAST(0 AS int) 
	end

END
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = -672
         Left = 0
      End
      Begin Tables = 
         Begin Table = "d"
            Begin Extent = 
               Top = 678
               Left = 38
               Bottom = 808
               Right = 211
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "c"
            Begin Extent = 
               Top = 678
               Left = 249
               Bottom = 774
               Right = 419
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'DataToExport'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'DataToExport'
GO
